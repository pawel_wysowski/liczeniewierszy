﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                char car;
                int count = 0;
                int work = 0;
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                var directory = Path.GetDirectoryName(path);
                using (StreamReader bs = new StreamReader(new FileStream(directory+@"\test.txt", FileMode.Open)))
                {

                    while (bs.Peek()>=0) {
                        car = (char)bs.Read();
                        

                        if (car == '\r' || car == '\n' || bs.Peek()==-1)
                        {
                            if (work>=1)
                            count++;
                            work = 0;
                        }
                        else work++;
                    }                    
                }
                Console.WriteLine(count);
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }
          
    }
}
